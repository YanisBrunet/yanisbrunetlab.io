
$(document).ready(function () {

var viewer       = document.querySelector('.viewer'),
  frame_count  = 21,
  offset_value = 480;

  var controller = new ScrollMagic.Controller();

  // build pinned scene
  new ScrollMagic.Scene({
    triggerElement: '#sticky',
    triggerHook: 0.15,
    duration: (frame_count * offset_value) + 'px',
  })
  .setPin('#sticky')
  .on('end', function() { document.location.href = "/home"; })
  .addTo(controller);


  // build step frame scene
  for (var i = 1, l = frame_count; i <= l; i++) {
    new ScrollMagic.Scene({
        triggerElement: '#sticky',
        offset: i * offset_value
      })
    
    .setClassToggle(viewer, ($(window).width() >= 960 ? 'frame' : 'framesmall') + i)
    .addTo(controller);
  }

  var lastoffset = scrollY,
      increaseScroll = true;

  // animation scroll
  if ($('body').is('.fixed-bg')) {

    window.onscroll = function(ev) {
 
      //check for end of scroll and redirect
      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        document.location.href = "/home";
      }
 
      // increase scroll speed
      if (increaseScroll) {
        newoffset = scrollY;
        scrollBy(0, 50 * (lastoffset < newoffset ? 1 : -1));
        lastoffset = newoffset;
       
        setTimeout(function(){ increaseScroll = false; }, 500);
        setTimeout(function(){ increaseScroll = true; }, 501);
      }
    };
  }

  new fullpage('#fullpage', {      //options 
      anchors: ["about", "work", "contact"]
  });

});
